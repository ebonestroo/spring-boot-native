If you want this as a regular java app and not the native image you will need to use the local profile in your run config


Run first: (running this as a maven goal in intellij will let you not set JAVA_HOME)

```
$ ./mvnw spring-boot:build-image -DskipTests
```


run second:

```
$ ./docker-compose up
```