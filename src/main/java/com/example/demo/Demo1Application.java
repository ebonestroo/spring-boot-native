package com.example.demo;

import io.netty.buffer.AbstractByteBufAllocator;
import io.netty.util.internal.PlatformDependent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.nativex.hint.NativeHint;

@SpringBootApplication
@Slf4j
public class Demo1Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo1Application.class, args);
    }

    @Bean
    public Queue queue() {
        return new Queue("graal");
    }

    @RabbitListener(id = "graal", queues = "graal")
    public void upperCaseIt(String in) {
        log.info("Received: {}", in);
    }

    @Bean
    public Queue queue2() {
        return new Queue("graal2");
    }

    @RabbitListener(id = "graal2", queues = "graal2")
    public void upperCaseIt2(String in) {
        log.info("Received: {}", in);
    }

    @Bean
    public Queue queue3() {
        return new Queue("graal3");
    }

    @RabbitListener(id = "graal3", queues = "graal3")
    public void upperCaseIt3(String in) {
        log.info("Received: {}", in);
    }

    @Bean
    public Queue queue4() {
        return new Queue("graal4");
    }

    @RabbitListener(id = "graal4", queues = "graal4")
    public void upperCaseIt4(String in) {
        log.info("Received: {}", in);
    }

    @Bean
    public Queue queue5() {
        return new Queue("graal5");
    }

    @RabbitListener(id = "graal5", queues = "graal5")
    public void upperCaseIt5(String in) {
        log.info("Received: {}", in);
    }

}
