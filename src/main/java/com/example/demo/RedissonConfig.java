package com.example.demo;

import io.netty.buffer.AbstractByteBufAllocator;
import io.netty.channel.ReflectiveChannelFactory;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.ReferenceCountUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.api.RemoteInvocationOptions;
import org.redisson.config.Config;
import org.redisson.remote.RemoteServiceAck;
import org.redisson.remote.RemoteServiceRequest;
import org.redisson.remote.RemoteServiceResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.nativex.hint.*;

import java.lang.reflect.Proxy;

@Configuration
@SerializationHint(types = {RemoteServiceRequest.class, RemoteInvocationOptions.class, RemoteServiceAck.class, RemoteServiceResponse.class})
@TypeHints({ //
        @TypeHint(types = AbstractByteBufAllocator.class, access = TypeAccess.DECLARED_METHODS),
        @TypeHint(types = ReflectiveChannelFactory.class, access = TypeAccess.DECLARED_METHODS),
        @TypeHint(
                typeNames = {
                        "io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueueProducerIndexField",
                        "io.netty.util.internal.shaded.org.jctools.queues.BaseMpscLinkedArrayQueueProducerFields"
                },
                fields = @FieldHint(name = "producerIndex", allowUnsafeAccess = true)
        ), //
        @TypeHint(
                typeNames = {
                        "io.netty.util.internal.shaded.org.jctools.queues.BaseMpscLinkedArrayQueueColdProducerFields",
                        "io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueueProducerLimitField"

                },
                fields = @FieldHint(name = "producerLimit", allowUnsafeAccess = true)
        ), //
        @TypeHint(
                typeNames = {
                        "io.netty.util.internal.shaded.org.jctools.queues.BaseMpscLinkedArrayQueueConsumerFields",
                        "io.netty.util.internal.shaded.org.jctools.queues.MpscArrayQueueConsumerIndexField"

                },
                fields = @FieldHint(name = "consumerIndex", allowUnsafeAccess = true)
        ), //
        @TypeHint(types = Proxy.class,  fields = @FieldHint(name = "h", allowUnsafeAccess = true)),
        @TypeHint(types = ReferenceCountUtil.class, access = TypeAccess.DECLARED_METHODS),
        @TypeHint(types = NioSocketChannel.class, methods = @MethodHint(name = "<init>"))}
)
public class RedissonConfig {
    @Value("${reddis.address}")
    private String address;
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setAddress(address)
                .setPassword("eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81");
        return Redisson.create(config);
    }
}
