package com.example.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("")
@Slf4j
public class TestController {

    private final RabbitTemplate rabbitTemplate;
    private final TestRepository testRepository;

    private final RedissonClient redissonClient;

    @GetMapping
    public TestDto test(){
        rabbitTemplate.convertAndSend("graal","Test");
        rabbitTemplate.convertAndSend("graal2","Test2");
        rabbitTemplate.convertAndSend("graal3","Test3");
        rabbitTemplate.convertAndSend("graal4","Test4");
        rabbitTemplate.convertAndSend("graal5","Test5");

        log.info("preLock ");
        RLock test = redissonClient.getLock("test");
        test.lock();
        log.info("lock aquired locked "+ test.isLocked());
        Test entity = new Test();
        entity.setText("Test ");
        Test save = testRepository.save(entity);
        test.unlock();
        log.info("lock released ");
        return new TestDto("Hallo wereld "+ save.getId() + " ");
    }
}
