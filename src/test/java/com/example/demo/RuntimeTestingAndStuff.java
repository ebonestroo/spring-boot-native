package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = RestTemplateAutoConfiguration.class)
@Slf4j
public class RuntimeTestingAndStuff {
    @Autowired
    RestTemplateBuilder restTemplateBuilder;

    @Test
    public void test() throws InterruptedException {
        RestTemplate restTemplate = restTemplateBuilder.build();

        Runnable thousandRequests = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 1000; i++) {
                    TestDto forObject = restTemplate.getForObject("http://localhost:8080/", TestDto.class);
                    log.info("{}", forObject);
                }
            }
        };
        Thread thread = new Thread(thousandRequests);
        thread.start();
        Thread thread1 = new Thread(thousandRequests);
        thread1.start();
        Thread thread2 = new Thread(thousandRequests);
        thread2.start();

        thread.join();
        thread1.join();
        thread2.join();
    }
}
